-- Creation of product table
CREATE TABLE IF NOT EXISTS scaleflex (
  name varchar(250) NOT NULL,
  sha1 varchar(250) NOT NULL,
  image_type varchar(250) NOT NULL,
  width INT NOT NULL,
  height INT NOT NULL,
  posting_date DATE NOT NULL
  PRIMARY KEY (name)
);