# CREATE POSTGRES ENV, DB, Tables
docker-compose up -d

# install needed packages, python3 and pip are prerequisite
pip3 install -r requirements.txt

# start flask server
python3 flask_app.py