# README #

### What is this repository for? ###

Flask application that users are able to post url to it and the web service can identify wether that url contains an image or not, if it does saves dimensions, sha1 value and type of the image into a postgres database.

Approriate responses are designed if there isn't an image in posted URL.

We are using docker-compose to create the Postrgres environment.

### How do I get set up? ###

Simply run:

```
sh start.sh
```

This will execute in order:

* Create Postgres environment
* Install all needed packages for the web service to run
* Start Flask

### How to use ? ###

After starting the application two endpoints would be able to be hit:

* First enpoint running on http://127.1.0.0:5000/send-image/<path:url> where user can send POST request with URL pointing to image and have the details of it (dimensions, sha1 value and type of the image) saved to DB. Appropriate response should be returned with message and status_code.

  Example:  
```yaml
{
    "message": "Data has been inserted into db",
    "status_code": 200
}
```
* Second endpoint running on http://127.1.0.0:5000/get_last_100_records where user can POST an empty request and receive back last 100 records of image dimensions sorted by most recent.

Example:
```yaml
[{"name": "https://miro.medium.com/proxy/1*0G5zu7CnXdMT9pGbYUTQLQ.png", "sha1": "023734b0ddc10664cb35bfe609eb4678738fdbca", "width": 438, "height": 245, "posting_date": "2022-02-21"}, {"name": "https://lp-cms-production.imgix.net/features/2018/02/Aleksander-Nevski-Cathedral-Sofia-6303650c1b02.jpg", "sha1": "81ff8643076fc5d0b4ab913f9688317d08d04336", "width": 1600, "height": 1200, "posting_date": "2022-02-21"}, {"name": "https://wallpaperaccess.com/full/3547002.jpg", "sha1": "f27d2b99a0beed379f220917456fa72e3dbb6096", "width": 1931, "height": 1211, "posting_date": "2022-02-21"}, {"name": "https://deeemm.com/images/flask-icon.png", "sha1": "1e6db6ea4d11ba7e70c6d269815360cf124919ee", "width": 180, "height": 161, "posting_date": "2022-02-20"}]
```

### Testing ###

Run: ```curl_tests.sh``` to run some tests using curl post

### Limitations and mistakes ###

* Time management, couldn't do the Bonus task.
* More testing, tested on some high res images but more was needed.
* Image format reading does not work perfectly.
* No async await implemented for high load.
* Perhaps more comments.