import json
from flask import Flask, request
import base64
from io import BytesIO
import requests
from hashlib import sha1
from PIL import Image
import psycopg2
import flask
import os
from datetime import datetime
import concurrent.futures
from tqdm import tqdm
from os import listdir
from os.path import isfile, join
import asyncio


# initialise connection with postgressql
from typing import Set

conn = psycopg2.connect(
    host="localhost",
    database="postgres",
    user="postgres",
    password="testtest")

cur = conn.cursor()

app = Flask(__name__)

# get absolute path to image_storage
UPLOAD_FOLDER = os.path.abspath("image_storage")
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg', 'gif']


# By using the <path: url> specifier, we ensure that the string that will come after send-image / is taken as a whole.
@app.route("/send-image/<path:url>", methods=['GET', 'POST'])
def image_check(url):
    """
    Steps in order
    1. check if image in url else return appropriate response
    2. get image data (sha1, dimensions and type) else return appropriate response
    3. check if image already in DB if not then push to DB else return appropriate response
    """

    # response schema
    response_json = {'status_code': int,
                     'message': str,
                     }

    # list of expected formats
    image_formats = ("image/png", "image/jpeg", "image/jpg")

    # check if url contains image
    r = requests.head(url)

    if r.headers["content-type"] in image_formats:

        # get sha1 value using hexdigest, first convert image into bytes and then sha1
        # get width and height after converting image into bytes
        #
        try:

            # image data schema
            image_data = {'name': 'unknown',
                          'sha1': f"{40 * '0'}",
                          'width': 0,
                          'height': 0,
                          'image_type': 'unknown'}

            # convert image to bytes
            response = requests.get(url)
            bytes_image = BytesIO(response.content)

            # convert to base64 as that's what hexdigest takes as arguments
            converted_image_to_base64 = base64.b64encode(bytes_image.read())
            h = sha1(converted_image_to_base64).hexdigest()

            # using PIL library read the width and height of the image
            scr = Image.open(bytes_image)

            image_data['width'], image_data['height'] = scr.size

            # populate dict with information
            image_data['name'] = str(url)
            image_data['sha1'] = str(h)
            image_data['image_type'] = r.headers["content-type"]

            try:
                cur.execute("insert into scaleflex (name, sha1, image_type, width, height, posting_date) "
                            "values (%s, %s, %s, %s, %s, %s)",
                            (image_data['name'],
                             image_data['sha1'],
                             image_data['image_type'],
                             image_data['width'],
                             image_data['height'],
                             datetime.today().strftime('%Y-%m-%d'),
                             ))

                # commit data to DB
                conn.commit()

                response_json['status_code'] = 200
                response_json['message'] = 'Data has been inserted into db'

                return response_json

            # raise an exception if url already in DB
            except Exception as e:

                response_json['status_code'] = 500
                response_json['message'] = f"Duplicate url, url is primary key for the database so it must be unique"

                return response_json

        # Catch any outstanding errors
        except Exception as e:

            response_json['status_code'] = 500
            response_json['message'] = f"Something went wrong: \n {e}"

            return response_json

    # No image in the URL
    else:

        response_json['status_code'] = 500
        response_json['message'] = "No image found in url"

        return response_json


# endpoint for quering last 100 records sorted by date
@app.route("/get_last_100_records", methods=['GET', 'POST'])
def get_last_100_records():
    """
    execute query that selects all data fields,
    returns all elements from table ordered by posted date, limit to 100
    """

    cur.execute(
        "SELECT json_agg(t) FROM (SELECT name, sha1, width, height, posting_date FROM scaleflex ORDER BY posting_date DESC LIMIT 100) t;")

    # fetch the query and return as json
    response_query = cur.fetchone()[0]

    return json.dumps(response_query)


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


async def process_image(image_filename):
    print(image_filename)
    # image data schema
    image_data = {'name': 'unknown',
                  'sha1': f"{40 * '0'}",
                  'width': 0,
                  'height': 0,
                  'image_type': 'unknown'}

    # convert image to bytes
    with open(f"{UPLOAD_FOLDER}/{image_filename}", "rb") as image:
        f = image.read()
        b = bytearray(f)

    bytes_image = BytesIO(f)

    converted_image_to_base64 = base64.b64encode(b)
    h = sha1(converted_image_to_base64).hexdigest()

    # convert to base64 as that's what hexdigest takes as arguments
    # using PIL library read the width and height of the image
    scr = Image.open(bytes_image)

    image_data['width'], image_data['height'] = scr.size
    # populate dict with information
    image_data['name'] = str(image_filename)
    image_data['sha1'] = str(h)
    image_data['image_type'] = '.' in image_filename and image_filename.rsplit('.', 1)[1].lower()

    try:
        cur.execute("insert into scaleflex (name, sha1, image_type, width, height, posting_date) "
                    "values (%s, %s, %s, %s, %s, %s)",
                    (image_data['name'],
                     image_data['sha1'],
                     image_data['image_type'],
                     image_data['width'],
                     image_data['height'],
                     datetime.today().strftime('%Y-%m-%d'),
                     ))

        # commit data to DB
        conn.commit()
    except Exception as e:
        if 'duplicate key ' in str(e):
            pass
        else:
            response_json = {'status_code': 500, 'message': f"Something is wrong \n{e}"}
            return response_json


async def main_async(image_filename):
    for image in image_filename:
        await asyncio.gather(process_image(image))


# endpoint for quering last 100 records sorted by date
@app.route("/run_bulk_images", methods=['GET', 'POST'])
def run_bulk_images():
    """
    execute query that selects all data fields,
    returns all elements from table ordered by posted date, limit to 100
    """
    response_json = {'status_code': 200, 'message': f"Success"}

    if flask.request.method == "POST":
        files = request.files.getlist("image")
    else:
        response_json['status_code'] = 500
        response_json['message'] = "Wrong type of request"
        return response_json

    def save_image(image):
        if allowed_file(image.filename):
            image.save(os.path.join(app.config['UPLOAD_FOLDER'], image.filename))
        else:
            print('not an image')

    print('Saving images to folder')
    with concurrent.futures.ThreadPoolExecutor() as executor:
        results = list(tqdm(executor.map(save_image,
                                         files), total=len(files)))

    image_file_list = [f for f in listdir(UPLOAD_FOLDER) if isfile(join(UPLOAD_FOLDER, f))]

    print('Putting images to db')
    asyncio.run(main_async(image_filename=image_file_list))

    return response_json


if __name__ == '__main__':
    app.run(host='0.0.0.0')
